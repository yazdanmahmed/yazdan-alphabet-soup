Word Search Solver
The Word Search Solver is a command-line program that helps you find words in a word search grid. The program takes an input 
file containing a word search grid and a list of words to find, then found words along with their positions in the grid.

Installation
Download the source code or Clone the repository.
Make sure you have Python 3 installed on your system.

Usage
Create an input file, use the following format:

<Grid Dimensions>
<Grid Characters>
<Words to Find>


<Grid Dimensions>: The number of rows and columns in the grid. For example, "5x5".
<Grid Characters>: Represents the characters in the grid, with each row on a new line. Characters in a row should be separated by spaces.
<Words to Find>: The words to be found, with each word on a new line.

Example:

5x5
A B C D E
F G H I J
K L M N O
P Q R S T
U V W X Y
CAT
DOG
RABBIT

Open a terminal or command prompt and navigate to the directory where the app is located.

Run the following command to execute the program:

python word_search.py <input_file>

Replace <input_file> with the path to your input file.

The program takes the file and processes, then outputs as shown in the examples below. 

Example:

Input file (input.txt):

7x7
C A R A O G H
R A M G E Y A
E N T M N A L
S E C R E T O
P P O R S T P
O N O S I A L
N D F T H O A
SECRET
CAR
HALO

Command: python word_search.py input.txt

Output:

SECRET 3:0 3:5
CAR 0:0 0:2
HALO 0:6 3:6

You can find more example grids in the Challenge-Yaz directory, Sample1, Sample2, and Sample 3
