import argparse

def find_words(file_path):
    # Read the input file
    with open(file_path, 'r') as file:
        # Read the grid dimensions
        dimensions = file.readline().strip().split('x')
        rows = int(dimensions[0])
        cols = int(dimensions[1])

        # Read the grid of characters
        grid = []
        for _ in range(rows):
            row = file.readline().strip().split(' ')
            grid.append(row)

        # Read the words to be found
        words = file.read().strip().split('\n')

    # Helper function to check if a word can be found starting from a given position
    def find_word(word, start_row, start_col, direction_row, direction_col):
        end_row = start_row + (len(word) - 1) * direction_row
        end_col = start_col + (len(word) - 1) * direction_col

        # Check if the word fits within the grid
        if end_row < 0 or end_row >= rows or end_col < 0 or end_col >= cols:
            return False

        # Check if the characters match
        for i in range(len(word)):
            row = start_row + i * direction_row
            col = start_col + i * direction_col
            if grid[row][col] != word[i]:
                return False

        return True

    # Search for each word in the grid
    results = []
    for word in words:
        found = False
        start_pos = None
        end_pos = None

        # Search horizontally
        for row in range(rows):
            for col in range(cols - len(word) + 1):
                if find_word(word, row, col, 0, 1):
                    start_pos = f"{row}:{col}"
                    end_pos = f"{row}:{col + len(word) - 1}"
                    found = True
                    break
                elif find_word(word, row, col + len(word) - 1, 0, -1):
                    start_pos = f"{row}:{col + len(word) - 1}"
                    end_pos = f"{row}:{col}"
                    found = True
                    break
            if found:
                break

        # Search vertically
        if not found:
            for row in range(rows - len(word) + 1):
                for col in range(cols):
                    if find_word(word, row, col, 1, 0):
                        start_pos = f"{row}:{col}"
                        end_pos = f"{row + len(word) - 1}:{col}"
                        found = True
                        break
                    elif find_word(word, row + len(word) - 1, col, -1, 0):
                        start_pos = f"{row + len(word) - 1}:{col}"
                        end_pos = f"{row}:{col}"
                        found = True
                        break
                if found:
                    break

        # Search diagonally (top-left to bottom-right)
        if not found:
            for row in range(rows - len(word) + 1):
                for col in range(cols - len(word) + 1):
                    if find_word(word, row, col, 1, 1):
                        start_pos = f"{row}:{col}"
                        end_pos = f"{row + len(word) - 1}:{col + len(word) - 1}"
                        found = True
                        break
                    elif find_word(word, row + len(word) - 1, col + len(word) - 1, -1, -1):
                        start_pos = f"{row + len(word) - 1}:{col + len(word) - 1}"
                        end_pos = f"{row}:{col}"
                        found = True
                        break
                if found:
                    break

        # Search diagonally (bottom-left to top-right)
        if not found:
            for row in range(len(word) - 1, rows):
                for col in range(cols - len(word) + 1):
                    if find_word(word, row, col, -1, 1):
                        start_pos = f"{row}:{col}"
                        end_pos = f"{row - len(word) + 1}:{col + len(word) - 1}"
                        found = True
                        break
                    elif find_word(word, row - len(word) + 1, col + len(word) - 1, 1, -1):
                        start_pos = f"{row - len(word) + 1}:{col + len(word) - 1}"
                        end_pos = f"{row}:{col}"
                        found = True
                        break
                if found:
                    break

        # Add the result to the list
        if found:
            result = f"{word} {start_pos} {end_pos}"
            results.append(result)

    # Print the results
    for result in results:
        print(result)


# Parse command-line arguments
parser = argparse.ArgumentParser(description='Find words in a word search grid.')
parser.add_argument('file', type=str, help='input file path')
args = parser.parse_args()

# Call the find_words function with the input file
find_words(args.file)
